.SUFFIXES: .html .odt .md

MD_FILES := $(wildcard *.md)
HTMLS := $(patsubst %.md, %.html, $(MD_FILES))
ODTS := $(patsubst %.md, %.odt, $(MD_FILES))

.md.html:
	multimarkdown $<   >   $@

.md.odt:
	multimarkdown -t odf $<   >   $@

convert: $(HTMLS) $(ODTS)

clean:
	rm $(HTMLS) $(ODTS)

all: convert
