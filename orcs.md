Title: Orcs Army List | Kings of War, 3rd edition (public release) | Epic fantasy armies clash on the fields of battle
Base Header Level: 1
Keywords: Fantasy, Mantic, 28mm, Wargame, Alessio Cavatore
CSS: http://warforge.org/kow.css
copyright: 2012 Mantic Games
This work is licensed under a Creative Commons License.
http://creativecommons.org/licenses/by-nc-sa/3.0/ 


![][KingsOfWar]

[KingsOfWar]: http://warforge.org/kow/i/kow-logo-horizontal.png "Kings of War"

# Orcs Army List

* Alignment: Evil
* Army Special Rules: All units have the Crushing Strength (1) special rule, or better if stated in their entry.

## Goblin Stabby Sneek

Mark a Krudger that is accompanied by a Sneek with an appropriate model. The Krudger then counts as being equipped with a throwing weapon. When you send the Sneek on a stabby mission, roll 3 dice to hit, regardless of the fi rer's Attacks. The Sneek always hits on 4+, regardless of modifiers. Roll to damage as normal. After this, roll a die. On a 3 or less the Sneek has been caught by the enemy and is removed. On a 4+ he goes, stabs and returns to the Krudger's side cackling with evil satisfaction.

## Goblin Zappy Sneak

Mark a Krudger accompanied by a Sneek with an appropriate model. The Krudger has the Zap (1) special rule. Each time you order the Zappy Sneek to zap someone, roll a die. On a 2+ the Sneek obeys, but if you roll a 1, he zaps the Krudger instead and disappears in a puff of rancid-smelling smoke (remove the model).

### Ax (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 | 5  | 4+ | -- | 5+ | 10 | 10/12 |  75 |
Regiment (15)              | 5  | 4+ | -- | 5+ | 10 | 12/14 |  90 |
Krudgiment (20)            | 5  | 4+ | -- | 5+ | 10 | 13/15 | 100 |
Horde (30)                 | 5  | 4+ | -- | 5+ | 20 | 17/19 | 150 |
Krudghorde (40)            | 5  | 4+ | -- | 5+ | 20 | 20/22 | 190 |

#### Options

* Banner (+15 points)
* Musician (+10 points)


### Greatax (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 | 5  | 3+ | -- | 4+ | 10 | 10/12 |  80 |
Regiment (20)              | 5  | 3+ | -- | 4+ | 10 | 13/15 | 105 |
Horde (40)                 | 5  | 3+ | -- | 4+ | 20 | 20/22 | 200 |

#### Special
* Crushing Strength (2)

#### Options

* Banner (+15 points)
* Musician (+10 points)


### Morax (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 | 5  | 3+ | -- | 4+ | 20 | 10/12 | 115 |
Regiment (20)              | 5  | 3+ | -- | 4+ | 20 | 13/15 | 150 |

#### Options

* Banner (+15 points)
* Musician (+10 points)


### Sniff (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (5)                  | 6  | 4+ | 5+ | 3+ |  5 |  9/11 |  50 |
Half-Regiment (10)         | 6  | 4+ | 5+ | 3+ | 10 | 10/12 |  90 |

#### Special
* Bows
* Vanguard

#### Options

* Musician (+10 points)


### Orclings* (Large Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 4  | 5+ | -- | 3+ |  5 |  7/10 |  25 |
Troop (3)                  | 4  | 5+ | -- | 3+ | 15 |  9-12 |  70 |
Regiment (6)               | 4  | 5+ | -- | 3+ | 30 | 12/15 | 135 |

#### Special
* Vicious
* Orclings do not have Crushing Strength


### Trolls* (Large Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 6  | 4+ | -- | 5+ |  3 |  9/11 |  50 |
Troop (3)                  | 6  | 4+ | -- | 5+ |  9 | 11/13 | 120 |
Regiment (6)               | 6  | 4+ | -- | 5+ | 18 | 14/16 | 230 |

#### Special
* Crushing Strength (2)
* Regeneration


### Gore Riders (Cavalry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (5)                  | 8  | 3+ | -- | 5+ |  8 | 10/12 |  85 |
Regiment (10)              | 8  | 3+ | -- | 5+ | 16 | 13/15 | 150 |

#### Special
* Crushing Strength (2)

#### Options

* Banner (+15 points)
* Musician (+10 points)


### Fight Wagons (Large Cavalry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 6  | 3+ | -- | 5+ |  6 |  9/11 |  70 |
Troop (3)                  | 6  | 3+ | -- | 5+ | 18 | 11/13 | 150 |
Regiment (6)               | 6  | 3+ | -- | 5+ | 36 | 14/16 | 285 |

#### Options

* Banner (+15 points)
* Musician (+10 points)


### Gore Chariots (Large Cavalry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 7  | 3+ | -- | 5+ |  3 |  9/11 |  55 |
Troop (3)                  | 7  | 3+ | -- | 5+ |  9 | 11/13 | 120 |
Regiment (6)               | 7  | 3+ | -- | 5+ | 18 | 14/16 | 230 |

#### Special
* Crushing Strength (2)

#### Options

* Banner (+15 points)
* Musician (+10 points)


### Flagger (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 5  | 3+ | -- | 4+ |  2 |  9/11 |  30 |

#### Special
* Individual
* Inspiring

#### Options
* Mount on a Gore,increasing Speed to 8 (+5 points)


### Krudger (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 5  | 3+ | -- | 5+ |  5 | 13/15 | 120 |

#### Special
* Crushing Strength (2)
* Individual
* Inspiring

#### Options

* Mount on a Gore,increasing Speed to 8 (+20 points)
* Goblin Stabby Sneak (+10 points)
* Goblin Zappy Sneak (+15 points)


### Krudger on Gore Chariot (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 7  | 3+ | -- | 5+ |  7 | 14/16 | 145 |

#### Special
* Crushing Strength (3)
* Inspiring

#### Options

* Goblin Stabby Sneak (+10 points)
* Goblin Zappy Sneak (+15 points)


### Krudger on Slasher (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 7  | 3+ | -- | 5+ | 10 | 16/18 | 200 |

#### Special
* Crushing Strength (3)
* Inspiring


### Krudger on Winged Slasher (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 10 | 3+ | -- | 5+ | 10 | 16/18 | 300 |

#### Special
* Crushing Strength (3)
* Fly
* Inspiring


### Giant (Monster)

| Unit Size                | Sp | Me | Ra | De |  At  | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :--: | :---: | :-: |
1                          | 7  | 4+ | -- | 5+ | 2D6* | 16/19 | 180 |

#### Special
* Crushing Strength (3)
* Roll for the number of Attacks every time you resolve a melee


### Wip the Half-cast(1) (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 6  | 4+ | -- | 4+ |  1 | 11/13 | 120 |

#### Special
* Elite
* Heal(3)
* Individual
* Zap!(5)

#### Notes

* Wip does not have Crushing Strength

---

## Credits
   
* Written by: Alessio Cavatore.

* Artwork: Roberto Cirillo, Sascha Duis, Des Hanley, Phil Moss, Stef
Kopinski, Jonas Springborg, Matt Lee.

* Thanks to: Beasts of War, Richard Lambert, Chris Palmer, Tom Pugh
and the Chelmsford Bunker, Josh Roberts, Tabletop Insider... oh, and
Ronnie too.

* Liberated by: Matt Lee and Rob Myers, with eternal gratitude to
  Ronnie Renton.

* Modified by: (see below and add your name here)

* Scenery from owner's collection.

* Produced by: River Horse (Europe)
  Ltd. [http://www.riverhorse.eu](http://www.riverhorse.eu).

* Published by: Mantic Entertainment
  Ltd. [http://www.manticgames.com](http://www.manticgames.com).

* Mantic wants you to change this game! <http://warforge.org>.

* Copyright &copy; Mantic Entertainment 2011. ISBN: 978-0-9569945-0-9

* This work is licensed under the Creative Commons
Attribution-NonCommercial-ShareAlike 3.0 Unported License. To view a
copy of this license, visit
[http://creativecommons.org/licenses/by-nc-sa/3.0/](http://creativecommons.org/licenses/by-nc-sa/3.0/)
or send a letter to Creative Commons, 444 Castro Street, Suite 900,
Mountain View, California, 94041, USA.

* We are releasing the Kings of War rules in this manner to allow
players to better understand their relationship with us, and to be
able to apply their own ideas, modifications and designs to the game
outside of the scope of our game.

---

# Making your own game based on Kings of War

This is a short guide intended for people who want to alter and
release a new game based on the Kings of War ruleset.

Firstly, thank you for considering Kings of War as the basis of your
rules. We have tried to make a game that is simple to pick up and
difficult to master, while keeping the rules short and fluid. We hope
that your game will continue this legacy.

## Under the license chosen by Mantic, you can:

* Alter and improve upon the rules of Kings of War, including adapting
  them into new games and altering them to make variants of Kings of
  War (historical, aquatic, modern, etc).

* Publish your new rules: forums, blogs, wikis, mailing lists, copies
  at your local club -- even printed copies can be made, and you can
  charge a nominal fee for the price of copying (a pound, a dollar, a
  euro, etc)

* Use [official graphics and photography from the Mantic Games website](http://www.manticgames.com/Trade/Images/Kings-of-War-Downloads.html)
  to illustrate your ruleset, providing credit is given.

## Under the conditions:

* Commercial usage forbidden: Expressly, Mantic means commercial usage
  in the sense of selling copies of the rules. A commercial website
  such as Beasts of War is free to make their own rules and publish
  them for their readers, but games based on this ruleset cannot be
  sold. There is some consideration being given to removing this
  restriction in future releases on the rules, and if you're
  interested in seeing this happen, please email info@manticgames.com
  and give your feedback. One nice side-effect of removing this
  restriction is that games will be compatible with the licensing of
  Wikipedia, allowing games to include graphics and other materials
  from Wikipedia, as well as being publishable ON Wikipedia.

* Share and share-alike: **Your rules must carry the same license as
  these rules, and anyone receiving a copy of the rules must have the
  same freedoms as you regards modifying the rules.** Mantic also asks
  that you keep the original 'Credits' page intact, and add your own
  name or names to it, but do not remove any names or companies from
  the page.

* Please see the [human-readable license
  summary](http://creativecommons.org/licenses/by-nc-sa/3.0/) and
  [full legal
  code](http://creativecommons.org/licenses/by-nc-sa/3.0/legalcode)
  for all the details.

## Get the files to make your own version of Kings of War:

The core rules of Kings of War can be found at
<http://warforge.org/kow/3.0/> -- here, you can download the rules and
army lists in a variety of formats suitable for modification.

1. OpenDocument Format -- this is ideal if you want to casually modify
the rules in a simple Word Processing document. LibreOffice is an
office suite that probably exists for your operating system -- head
over to <http://libreoffice.org> and download a copy of this free
software. Once installed, you can simply open and change the
"KingsOfWar.odt" file you downloaded from Warforge.

2. HTML -- this format is more suitable to online publishing, but can
be easily edited in any text editor, or visual HTML editor. The HTML
version is supplied as a ZIP archive, containing all the images and
CSS files used for the online version of Warforge.

3. Markdown -- this is the ultimate version for any would-be game
hacker. This is the complete plain-text source file for Kings of War
that is used to generate the other files and the Warforge website. If
you're unfamiliar with running programs from a Command Prompt or
Terminal, you may struggle to use the Markdown version of Kings of War
effectively.

With any version of the game, the most important thing is to have fun
and make your own new game that you enjoy playing with friends! Do not
let a desire to produce something too fancy looking detract from
playing the game and rolling bucketloads of dice.

## Naming your new game:

* If your game is a variant of Kings of War, such as a Historical or
  Underwater version of the game, Mantic expressly permits you to
  include the name "Kings of War" in the title of your game, if you
  want to.

* For games unrelated to Kings of War, please *do not* use the Kings
  of War name in your game title.

## Credit where credit due!

* One more thing to do before you send out your great new game -- put
  YOUR name on it! Make sure that your version of Kings of War --
  whatever it may be called -- has the complete license and credit
  section left intact. You'll notice we left a space there for you! If
  you're modifying a version that someone else has already modified,
  simply add your name underneath theirs.

* Be sure to leave an email address or forum handle, and where
  possible use your real name, rather than just an alias. 

## Publishing your game:

* Once you're happy with your game rules, you might want to make it
look pretty. For this, we recommend [Scribus](http://scribus.org) --
Scribus is a piece of free software, available for Windows, Mac and
GNU/Linux, and will let you lay out text and graphics in an attractive
manner. It can make PDFs files suitable for littering the floor of
your local gaming store, and at a variety of sizes suitable for
obscuring the covers of even the heaviest of overpriced rulebooks. The
Markdown tool mentioned previously can also produce very stylish
output, but requires a little more technical knowledge.

## Your feedback

Please feel free to send me your feedback on the process of making
your own games based on Kings of War, either in the [Mantic
forum](http://forum.manticblog.com/), other forums (Username: mattl),
the Mantic subreddit --
[http://mantic.reddit.com](http://mantic.reddit.com) or in-person once
Kings of War rises up and becomes the most popular fantasy wargame
around. I hope you enjoy what represents eighteen months of emails,
phone calls, legal discussions, ramifications, 2am brainstorming,
private messages, pub chats, hangover chats, demo games and dice
rolling.

Matt Lee, Boston, MA -- June 2012.

PS. If you're reading past this point in the Markdown or HTML
versions, be prepared for a bunch of computer junk -- this actually
marks the document with the rights set out by Mantic. It's pretty
important!

<!-- // leave this here, please
<?xpacket begin='' id=''?>
<x:xmpmeta xmlns:x='adobe:ns:meta/'>
  <rdf:RDF xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'>
    <rdf:Description rdf:about=''
		     xmlns:xapRights='http://ns.adobe.com/xap/1.0/rights/'>
      <xapRights:Marked>True</xapRights:Marked>
    </rdf:Description>
    <rdf:Description rdf:about=''
		     xmlns:xapRights='http://ns.adobe.com/xap/1.0/rights/'
		     >
      <xapRights:UsageTerms>
	<rdf:Alt>

	  <rdf:li xml:lang='x-default' >This work is licensed under a &lt;a rel=&#34;license&#34; href=&#34;http://creativecommons.org/licenses/by-nc-sa/3.0/&#34;&gt;Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License&lt;/a&gt;.</rdf:li>
	  <rdf:li xml:lang='en_US' >This work is licensed under a &lt;a rel=&#34;license&#34; href=&#34;http://creativecommons.org/licenses/by-nc-sa/3.0/&#34;&gt;Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License&lt;/a&gt;.</rdf:li>

	  <rdf:li xml:lang='en' >This work is licensed under a &lt;a rel=&#34;license&#34; href=&#34;http://creativecommons.org/licenses/by-nc-sa/3.0/&#34;&gt;Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License&lt;/a&gt;.</rdf:li>
	  </rdf:Alt>
      </xapRights:UsageTerms>
    </rdf:Description>

    <rdf:Description rdf:about=''
      xmlns:cc='http://creativecommons.org/ns#'>
      <cc:license rdf:resource='http://creativecommons.org/licenses/by-nc-sa/3.0/'/>
    </rdf:Description>
    
  </rdf:RDF>
</x:xmpmeta>
<?xpacket end='r'?> // end of Kings of War rules, Happy Hacking -->
