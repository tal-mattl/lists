Title: Twilight Kin Army List | Kings of War, 3rd edition (public release) | Epic fantasy armies clash on the fields of battle
Base Header Level: 1
Keywords: Fantasy, Mantic, 28mm, Wargame, Alessio Cavatore
CSS: http://warforge.org/kow.css
copyright: 2012 Mantic Games
This work is licensed under a Creative Commons License.
http://creativecommons.org/licenses/by-nc-sa/3.0/ 

![][KingsOfWar]

[KingsOfWar]: http://warforge.org/kow/i/kow-logo-horizontal.png "Kings of War"

# Twilight Kin Army List

* Alignment: Evil
* Army Special Rules: All units are Vicious.

### Twilight Spearmen (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 |  6 | 4+ | -- | 4+ | 10 | 11/13 |  85 |
Regiment (20)              |  6 | 4+ | -- | 4+ | 10 | 14/16 | 110 |
Horde (40)                 |  6 | 4+ | -- | 4+ | 20 | 21/23 | 210 |

Special: Phalanx

#### Twilight Spearmen options

* Banner (+15 points)
* Musician (+10 points)

### Darksome Guard (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (5)                  |  6 | 3+ | -- | 4+ |  5 | 10/12 |  50 |
Half-Regt. (10)            |  6 | 3+ | -- | 4+ | 10 | 11/13 |  90 |
Regiment (20)              |  6 | 3+ | -- | 4+ | 10 | 14/16 | 120 |

Special: Crushing Strength (1) 

#### Darksome Guard options

* Banner (+15 points)
* Musician (+10 points)

### Buccaneers (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 |  6 | 4+ | 4+ | 4+ | 15 | 11/13 | 115 |
Regiment (20)              |  6 | 4+ | 4+ | 4+ | 15 | 14/16 | 150 |
Horde (40)                 |  6 | 4+ | 4+ | 4+ | 30 | 21/23 | 285 |

Special: Hand crossbows (treat as thrown weapons)

#### Buccaneers options

* Banner (+15 points)
* Musician (+10 points)

### Blade-Dancers (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (5)                  |  7 | 3+ | -- | 3+ | 10 | 10/12 |  60 |
Half-Regt. (10)            |  7 | 3+ | -- | 3+ | 20 | 11/13 | 110 |
Regiment (20)              |  7 | 3+ | -- | 3+ | 25 | 14/16 | 160 |

Special: This unit always has a Musician (cost already included), Headstrong

#### Blade-Dancers options

* Banner (+15 points)

### Twilight Crossbowmen (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 |  6 | 4+ | 4+ | 4+ | 10 | 11/13 | 120 |
Regiment (20)              |  6 | 4+ | 4+ | 4+ | 10 | 14/16 | 160 |
Horde (40)                 |  6 | 4+ | 4+ | 4+ | 20 | 21/23 | 305 |

Special: Light crossbows (treat as bows)

#### Twilight Crossbowmen options

* Banner (+15 points)
* Musician (+10 points)

### Gargoyles* (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (5)                  | 10 | 4+ | -- | 3+ |  5 |  9/11 |  50 |
Half-Regt. (10)            | 10 | 4+ | -- | 3+ | 10 | 10/12 |  90 |

Special: Fly, Regeneragion

### Shadows (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 |  6 | 4+ | 4+ | 3+ | 10 | 11/13 | 105 |
Regiment (20)              |  6 | 4+ | 4+ | 3+ | 10 | 14/16 | 140 |

Special: Light crossbows (treat as bows), Nimble & Vanguard (Troop only)

#### Shadows options

* Banner (+15 points)
* Musician (+10 points)

### Dark Knights (Cavalry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (5)                  |  8 | 3+ | -- | 5+ |  9 | 11/13 |  95 |
Regiment (10)              |  8 | 3+ | -- | 5+ | 18 | 14/16 | 175 |

Special: Crushing Strength (2)

#### Dark Knights options

* Banner (+15 points)
* Musician (+10 points)

### Heralds of Woe (Cavalry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (5)                  | 10 | 4+ | 4+ | 3+ |  5 | 11/13 |  90 |
Regiment (10)              | 10 | 4+ | 4+ | 3+ | 10 | 14/16 | 160 |

Special: Light crossbows (treat as bows), Nimble

#### Heralds of Woe options

* Musician (+10 points)

### Abyssal Riders* (Large Cavalry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  7 | 4+ | -- | 5+ |  4 | 10/12 |  60 |
Troop (3)                  |  7 | 4+ | -- | 5+ | 12 | 12/14 | 140 |
Regiment (6)               |  7 | 4+ | -- | 5+ | 24 | 15/17 | 265 |

Special: Crushing Strength (1), Regeneration

#### Abyssal Riders* options

Troops and regiments can have:
* Banner (+15 points)
* Musician (+10 points)

### Lower Abyssals* (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 |  5 | 4+ | -- | 4+ | 20 | 11/13 | 115 |
Regiment (20)              |  5 | 4+ | -- | 4+ | 20 | 14/16 | 150 |
Horde (40)                 |  5 | 4+ | -- | 4+ | 40 | 21/23 | 285 |

Special: Regeneration

#### Lower Abyssals* options

* Banner (+15 points)
* Musician (+10 points)

### Twilight Bolt Thrower (War Engine)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  6 | -- | 4+ | 4+ |  2 | 10/12 |  75 |

Special: Blast (D3), Piercing (2)

### Twilight Dragon Breath (War Engine)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  6 | -- | -- | 4+ |  * | 10/12 |  60 |

Special: Breath Attack (20)

### Dark Lord (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  6 | 3+ | -- | 5+ |  5 | 14/16 | 120 |

Special: Crushing Strength (1), Individual, Inspiring

#### Dark Lord options

* Mount on a horse, increasing Speed to 9 (+20 points), or
* Mount on a Black Pegasus, increasing Speed to 10 and gaining Fly, but losing the Individual special rule (+40 points)

### Dark Lord on Dragon (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 10 | 3+ | -- | 5+ |  8 | 17/19 | 350 |

Special: Breath Attack (15), Crushing Strength (3), Fly, Inspiring

### Lordling on Winged Abyssal (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 10 | 3+ | -- | 5+ |  5 | 13/15 | 120 |

Special: Crushing Strength (2), Fly

### High Priestess of the Abyss (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  7 | 4+ | -- | 4+ |  1 | 11/13 | 110 |

Special: Breath Attack (10), Individual, Zap! (5)

#### High Priestess of the Abyss options

* Mount on a horse, increasing Speed to 9 (+10 points), or
* Mount on a Black Pegasus, increasing Speed to 10 and gaining Fly, but losing the Individual special rule (+40 points)

### Dark Avenger (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  6 | 3+ | -- | 5+ |  3 | 11/13 |  60 |

Special: Crushing Strength (1), Individual

#### Dark Avenger options

* Mount on a horse, increasing Speed to 9 (+10 points), or
* Mount on a Black Pegasus, increasing Speed to 10 and gaining Fly, but losing the Individual special rule (+20 points)

### Twilight Army Standard Bearer (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  6 | 4+ | -- | 4+ |  1 | 10/12 |  30 |

Special: Individual, Inspiring

#### Twilight Army Standard Bearer options

* Mount on a horse, increasing Speed to 9 (+5 points)

### Abyssal Fiend (Monster)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  7 | 4+ | -- | 5+ |  7 | 19/21 | 325 |

Special: Crushing Strength (3), Regeneration

#### Abyssal Fiend options

* Can have wings, gaining Fly and increasing Speed to 10, but reducing Defence to 4+ (+25 points)
* Can have the Breath Attack (10) special rule (+25 points)
* Can have the Zap! (5) special rule (+25 points)

### Death Stalker Simulacre(1) (Monster)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  6 | 3+ | -- | 6+ | 10 | 20/22 | 375 |

Special: Crushing Strength (3), Elite

### Assassin (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  6 | 3+ | 3+ | 4+ |  4 | 11/13 |  90 |

Special: Thrown weapons, Crushing Strength (1), Vanguard, Individual, Piercing (1), Stealthy

---

## Credits
   
* Written by: Alessio Cavatore.

* Artwork: Roberto Cirillo, Sascha Duis, Des Hanley, Phil Moss, Stef
Kopinski, Jonas Springborg, Matt Lee.

* Thanks to: Beasts of War, Richard Lambert, Chris Palmer, Tom Pugh
and the Chelmsford Bunker, Josh Roberts, Tabletop Insider... oh, and
Ronnie too.

* Liberated by: Matt Lee and Rob Myers, with eternal gratitude to
  Ronnie Renton.

* Modified by: (see below and add your name here)

* Scenery from owner's collection.

* Produced by: River Horse (Europe)
  Ltd. [http://www.riverhorse.eu](http://www.riverhorse.eu).

* Published by: Mantic Entertainment
  Ltd. [http://www.manticgames.com](http://www.manticgames.com).

* Mantic wants you to change this game! <http://warforge.org>.

* Copyright &copy; Mantic Entertainment 2011. ISBN: 978-0-9569945-0-9

* This work is licensed under the Creative Commons
Attribution-NonCommercial-ShareAlike 3.0 Unported License. To view a
copy of this license, visit
[http://creativecommons.org/licenses/by-nc-sa/3.0/](http://creativecommons.org/licenses/by-nc-sa/3.0/)
or send a letter to Creative Commons, 444 Castro Street, Suite 900,
Mountain View, California, 94041, USA.

* We are releasing the Kings of War rules in this manner to allow
players to better understand their relationship with us, and to be
able to apply their own ideas, modifications and designs to the game
outside of the scope of our game.

---

# Making your own game based on Kings of War

This is a short guide intended for people who want to alter and
release a new game based on the Kings of War ruleset.

Firstly, thank you for considering Kings of War as the basis of your
rules. We have tried to make a game that is simple to pick up and
difficult to master, while keeping the rules short and fluid. We hope
that your game will continue this legacy.

## Under the license chosen by Mantic, you can:

* Alter and improve upon the rules of Kings of War, including adapting
  them into new games and altering them to make variants of Kings of
  War (historical, aquatic, modern, etc).

* Publish your new rules: forums, blogs, wikis, mailing lists, copies
  at your local club -- even printed copies can be made, and you can
  charge a nominal fee for the price of copying (a pound, a dollar, a
  euro, etc)

* Use [official graphics and photography from the Mantic Games website](http://www.manticgames.com/Trade/Images/Kings-of-War-Downloads.html)
  to illustrate your ruleset, providing credit is given.

## Under the conditions:

* Commercial usage forbidden: Expressly, Mantic means commercial usage
  in the sense of selling copies of the rules. A commercial website
  such as Beasts of War is free to make their own rules and publish
  them for their readers, but games based on this ruleset cannot be
  sold. There is some consideration being given to removing this
  restriction in future releases on the rules, and if you're
  interested in seeing this happen, please email info@manticgames.com
  and give your feedback. One nice side-effect of removing this
  restriction is that games will be compatible with the licensing of
  Wikipedia, allowing games to include graphics and other materials
  from Wikipedia, as well as being publishable ON Wikipedia.

* Share and share-alike: **Your rules must carry the same license as
  these rules, and anyone receiving a copy of the rules must have the
  same freedoms as you regards modifying the rules.** Mantic also asks
  that you keep the original 'Credits' page intact, and add your own
  name or names to it, but do not remove any names or companies from
  the page.

* Please see the [human-readable license
  summary](http://creativecommons.org/licenses/by-nc-sa/3.0/) and
  [full legal
  code](http://creativecommons.org/licenses/by-nc-sa/3.0/legalcode)
  for all the details.

## Get the files to make your own version of Kings of War:

The core rules of Kings of War can be found at
<http://warforge.org/kow/3.0/> -- here, you can download the rules and
army lists in a variety of formats suitable for modification.

1. OpenDocument Format -- this is ideal if you want to casually modify
the rules in a simple Word Processing document. LibreOffice is an
office suite that probably exists for your operating system -- head
over to <http://libreoffice.org> and download a copy of this free
software. Once installed, you can simply open and change the
"KingsOfWar.odt" file you downloaded from Warforge.

2. HTML -- this format is more suitable to online publishing, but can
be easily edited in any text editor, or visual HTML editor. The HTML
version is supplied as a ZIP archive, containing all the images and
CSS files used for the online version of Warforge.

3. Markdown -- this is the ultimate version for any would-be game
hacker. This is the complete plain-text source file for Kings of War
that is used to generate the other files and the Warforge website. If
you're unfamiliar with running programs from a Command Prompt or
Terminal, you may struggle to use the Markdown version of Kings of War
effectively.

With any version of the game, the most important thing is to have fun
and make your own new game that you enjoy playing with friends! Do not
let a desire to produce something too fancy looking detract from
playing the game and rolling bucketloads of dice.

## Naming your new game:

* If your game is a variant of Kings of War, such as a Historical or
  Underwater version of the game, Mantic expressly permits you to
  include the name "Kings of War" in the title of your game, if you
  want to.

* For games unrelated to Kings of War, please *do not* use the Kings
  of War name in your game title.

## Credit where credit due!

* One more thing to do before you send out your great new game -- put
  YOUR name on it! Make sure that your version of Kings of War --
  whatever it may be called -- has the complete license and credit
  section left intact. You'll notice we left a space there for you! If
  you're modifying a version that someone else has already modified,
  simply add your name underneath theirs.

* Be sure to leave an email address or forum handle, and where
  possible use your real name, rather than just an alias. 

## Publishing your game:

* Once you're happy with your game rules, you might want to make it
look pretty. For this, we recommend [Scribus](http://scribus.org) --
Scribus is a piece of free software, available for Windows, Mac and
GNU/Linux, and will let you lay out text and graphics in an attractive
manner. It can make PDFs files suitable for littering the floor of
your local gaming store, and at a variety of sizes suitable for
obscuring the covers of even the heaviest of overpriced rulebooks. The
Markdown tool mentioned previously can also produce very stylish
output, but requires a little more technical knowledge.

## Your feedback

Please feel free to send me your feedback on the process of making
your own games based on Kings of War, either in the [Mantic
forum](http://forum.manticblog.com/), other forums (Username: mattl),
the Mantic subreddit --
[http://mantic.reddit.com](http://mantic.reddit.com) or in-person once
Kings of War rises up and becomes the most popular fantasy wargame
around. I hope you enjoy what represents eighteen months of emails,
phone calls, legal discussions, ramifications, 2am brainstorming,
private messages, pub chats, hangover chats, demo games and dice
rolling.

Matt Lee, Boston, MA -- June 2012.

PS. If you're reading past this point in the Markdown or HTML
versions, be prepared for a bunch of computer junk -- this actually
marks the document with the rights set out by Mantic. It's pretty
important!

<!-- // leave this here, please
<?xpacket begin='' id=''?>
<x:xmpmeta xmlns:x='adobe:ns:meta/'>
  <rdf:RDF xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'>
    <rdf:Description rdf:about=''
		     xmlns:xapRights='http://ns.adobe.com/xap/1.0/rights/'>
      <xapRights:Marked>True</xapRights:Marked>
    </rdf:Description>
    <rdf:Description rdf:about=''
		     xmlns:xapRights='http://ns.adobe.com/xap/1.0/rights/'
		     >
      <xapRights:UsageTerms>
	<rdf:Alt>

	  <rdf:li xml:lang='x-default' >This work is licensed under a &lt;a rel=&#34;license&#34; href=&#34;http://creativecommons.org/licenses/by-nc-sa/3.0/&#34;&gt;Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License&lt;/a&gt;.</rdf:li>
	  <rdf:li xml:lang='en_US' >This work is licensed under a &lt;a rel=&#34;license&#34; href=&#34;http://creativecommons.org/licenses/by-nc-sa/3.0/&#34;&gt;Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License&lt;/a&gt;.</rdf:li>

	  <rdf:li xml:lang='en' >This work is licensed under a &lt;a rel=&#34;license&#34; href=&#34;http://creativecommons.org/licenses/by-nc-sa/3.0/&#34;&gt;Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License&lt;/a&gt;.</rdf:li>
	  </rdf:Alt>
      </xapRights:UsageTerms>
    </rdf:Description>

    <rdf:Description rdf:about=''
      xmlns:cc='http://creativecommons.org/ns#'>
      <cc:license rdf:resource='http://creativecommons.org/licenses/by-nc-sa/3.0/'/>
    </rdf:Description>
    
  </rdf:RDF>
</x:xmpmeta>
<?xpacket end='r'?> // end of Kings of War rules, Happy Hacking -->
