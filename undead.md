Title: Undead Army List | Kings of War, 3rd edition (public release) | Epic fantasy armies clash on the fields of battle
Base Header Level: 1
Keywords: Fantasy, Mantic, 28mm, Wargame, Alessio Cavatore
CSS: http://warforge.org/kow.css
copyright: 2012 Mantic Games
This work is licensed under a Creative Commons License.
http://creativecommons.org/licenses/by-nc-sa/3.0/ 

![][KingsOfWar]

[KingsOfWar]: http://warforge.org/kow/i/kow-logo-horizontal.png "Kings of War"

# Undead Army List

* Alignment: Evil
* Army Special Rules: All units are Evil Dead.

## Evil Dead

The greatest horror of fighting the Undead is that your fallen
comrades rise immediately back up to join the ranks of the enemy.

Every time the unit manages to inflict one or more points of damage
against an enemy in melee, it regains a single point of damage that it
has previously suffered.

## Dark Surge (n)

Forward my minions! Forward! Muahahahaha!

The unit has a ranged attack that can only target friendly Shambling
units. You roll (n) dice for this ranged attack rather than using the
Attacks value of the unit. This attack has a range of 12" and always
hits on 4+, regardless of any modifier. For every hit 'inflicted', the
friendly unit moves straight forward a full inch (stopping 1" from
friendly units). If this movement brings it into contact with an enemy
unit, treat this as a successful charge. However, the charged unit
will not take any Nerve tests for damage it has taken in that Shoot
phase.

---

### Undead Giant Rats (or are they Dogs?)

These creatures follow the shambling hordes into battle, their
venomous bites helping to spread the curse of the living death.

Mark a unit that has been equipped with Undead Giant Rats (or are they
Dogs?) with one or more such models. This increases the effects of the
Evil Dead rule -- the unit recovers one point of damage it has suffered
per point of damage it inflicts in melee, rather than a single
one. However, at the end of each melee in which the unit fights, roll
a die. On a result of 1-3 the Rats (Dogs?) have been trampled in the
melee -- remove the Rat (Dog?) models, and the unit loses this ability.

### Revenants (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 | 5  | 4+ | -- | 5+ | 10 |  -/14 |  85 |
Regiment (20)              | 5  | 4+ | -- | 5+ | 10 |  -/17 | 110 |
Horde (40)                 | 5  | 4+ | -- | 5+ | 20 |  -/24 | 210 |

* Special: Shambling

#### Revenants options

* Banner (+15 pts)
* Musician (+10 pts)
* Exchange shields for two-handed weapons for free (lower Defence to
  4+, gain Crushing Strength (1))

Revenants are the reanimated rests of elite fighters, belonging to
Guard regiments and other troops that were better equipped and trained
than the average. This results in tougher, more skilled and more
resilient undead warriors.

### Wraiths  (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 | 10 | 4+ | -- | 6+ |  5 |  -/12 | 110 |
Half-Regiment (10)         | 10 | 4+ | -- | 6+ | 10 |  -/13 | 200 |
Regiment (20)              | 10 | 4+ | -- | 6+ | 10 |  -/16 | 265 |

* Special: Crushing Strength (1), Fly, Shambling

The souls of the most powerful enemies of the Vampires and
Necromancers are cursed to a ghostly quasi-existence, serving their
masters in death.

### Skeletons (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 | 5  | 5+ | -- | 4+ | 10 |  -/13 |  55 |
Regiment (20)              | 5  | 5+ | -- | 4+ | 10 |  -/16 |  70 |
Horde (40)                 | 5  | 5+ | -- | 4+ | 20 |  -/23 | 135 |

* Special: Shambling

#### Skeletons options

* Banner (+15 pts)
* Musician (+10 pts)
* Undead Giant Rats (Dogs?) (+30)
* Take Spears, gaining Phalanx:
 * +5 pts (Troops)
 * +15 pts (Regiments)
 * +25 pts (Hordes)

The Necromancers and Vampires can rely on the Skeletons'
unquestionable esprit de corpse.

### Skeleton Archers  (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 | 5  | 6+ | 5+ | 3+ | 10 |  -/13 |  55 |
Regiment (20)              | 5  | 6+ | 5+ | 3+ | 10 |  -/16 |  70 |
Horde (40)                 | 5  | 6+ | 5+ | 3+ | 20 |  -/23 | 135 |

* Special: Bows, Shambling

#### Skeleton Archers options

* Banner (+15 pts)
* Musician (+10 pts)
* Undead Giant Rats (Dogs?) (+30)

If a creature was a good shot in life, it can be raised from the grave
with a modicum of its former skill. It's not a coincidence that most
skeleton archers were once Elves.

### Mummies (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 |  4 | 4+ | -- | 6+ |  5 |  -/14 | 120 |
Half-Regiment (10)         |  4 | 4+ | -- | 6+ | 10 |  -/17 | 220 |
Regiment (20)              |  4 | 4+ | -- | 6+ | 10 |  -/24 | 295 |

* Special: Crushing Strength (2), Regeneration, Shambling

The embalmed corpses of these ancient warriors from the southern
deserts are slow, but almost impossible to destroy.

### Wights (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (3)                  |  5 | 4+ | -- | 5+ |  9 |  -/15 | 180 |

* Special: Crushing Strength (4), Vicious, Shambling

The malevolent Wights that infest the mist-clad barrows of the
wilderness were once kings of ancient realms.

### Zombies (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Regiment (20)              |  5 | 5+ | -- | 3+ | 15 |  -/15 |  65 |
Horde (30)                 |  5 | 5+ | -- | 3+ | 15 |  -/15 |  65 | 
Swarm (40)                 |  5 | 5+ | -- | 3+ | 15 |  -/15 |  65 |
Endless swarm (60)         |  5 | 5+ | -- | 3+ | 15 |  -/15 |  65 |

* Special: Shambling

#### Zombies options

* Banner (+15 pts)
* Musician (+10 pts)

Zombies are freshly raised corpses, and their numbers grow
exponentially during a campaign as the bodies of felled enemy soldiers
and civilian victims alike swell their ranks.

### Ghouls (Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (10)                 | 6  | 4+ | -- | 3+ | 10 |  9/11 |  60 |
Regiment (20)              | 6  | 4+ | -- | 3+ | 10 | 12/14 |  80 |
Horde (40)                 | 6  | 4+ | -- | 3+ | 20 | 19/21 | 150 |

The ghouls are not strictly walking dead, but rather deranged
cannibals and eaters of the dead that slowly lose their minds to the
horror of their lives, turning into savage creatures half-human,
half-undead.

### Werewolves (Large Infantry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 9  | 3+ | -- | 6+ |  3 | 10/12 |  70 |
Troop (3)                  | 9  | 3+ | -- | 6+ |  9 | 12/14 | 150 |
Regiment (6)               | 9  | 3+ | -- | 6+ | 18 | 15/17 | 285 |

* Special: Crushing Strength (1), Nimble

The curse of Lycanthropy turns its victims into huge wolf-hybrids,
nigh-invulnerable to most weapons and fighting, with speed and
strength beyond the limits of mortal flesh.

### Soul Reavers (Cavalry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (5)                  | 8  | 3+ | -- | 6+ | 13 | 12/14 | 185 |
Regiment (10)              | 8  | 3+ | -- | 6+ | 26 | 15/17 | 335 |

* Special: Crushing Strength (2)

#### Soul Reavers options

* Banner (+15 pts)
* Musician (+10 pts)

The Soul Reavers are composed exclusively of Vampire knights, the most
formidable warriors amongst the living dead -- not many enemies are
brave enough to stand their ground against one of their devastating
charges.

### Revenant Cavalry (Cavalry)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
Troop (5)                  | 8  | 4+ | -- | 5+ |  8 |  -/14 |  85 |
Regiment (10)              | 8  | 4+ | -- | 5+ | 16 |  -/17 | 155 |

* Special: Crushing Strength (2), Shambling

#### Revenant Cavalry options

* Banner (+15 pts)
* Musician (+10 pts)

The knights that have fallen fighting the undead are often condemned
to renege their vows in the most odious of ways, feasting on the souls
of those they had sworn to defend in life.

### Balefire Catapult  (War Engine)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 5  | -- | 5+ | 4+ |  1 |  -/12 |  65 |

* Special: Blast (2D6), Indirect Fire, Piercing (3), Shambling

Certainly not precise or fast, the skeletons crewing these war
machines are nevertheless more reliable than their living counterparts
and totally relentless in their occupation.

### Mhorgoth the Faceless (1)  (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 10 | 4+ | -- | 6+ |  5 | 17/19 | 400 |

* Special: Crushing Strength (1), Breath Attack (15), Dark Surge (12),
  Fly, Heal (6), Inspiring, Zap! (6), Regeneration

A curse upon the whole world, Mhorgoth the Faceless, the greatest of
Necromancers, rises again to continue his unholy quest -- turning all
living things into undead abominations under his control.

### Undead Army Standard Bearer  (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  5 | 4+ | -- | 4+ |  1 |  -/12 |  25 |

* Special: Shambling, Individual, Inspiring

#### Undead Army Standard Bearer options

* Mount on an undead horse, increasing speed to 8 (+5 pts)

Most undead are uninterested in honour and duty, but the sorceries
that imbue the gory standards of their armies are a source of unliving
energy that sustains the minions of the Undead Lords.

### Vampire on Undead Pegasus (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          | 10 | 3+ | -- | 5+ | 10 | 14/16 | 285 |

* Special: Crushing Strength (2), Dark Surge (3), Fly, Heal (3),
  Inspiring, Zap! (3)

The sight of one of these mighty creatures soaring above the
battlefield wielding its powerful magic is often enough to spread
blind panic amongst the enemy.

### Vampire Lord (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  7 | 3+ | -- | 6+ |  8 | 14/16 | 275 |

* Special: Crushing Strength (2), Dark Surge (3), Heal (3),
  Individual, Inspiring, Zap! (3)

#### Vampire Lord options

* Mount on an undead horse, increasing Speed to 8 (+20 pts)

These most powerful undead lords combine an unparalleled fighting
ability with great necromantic powers.

### Cursed Pharaoh (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  4 | 4+ | -- | 6+ |  5 | 18/20 | 200 |

* Special: Crushing Strength (2), Dark Surge (6) (Mummies only),
  Individual, Inspiring (Mummies only), Regeneration

Driven by a willpower that conquered death itself centuries past,
these fallen nobles are almost unstoppable in their unquenchable
wrath. Great is their ire and the vengeance they exact on those that
are greedy and foolish enough to dare violate their majestic burial
complexes.

### Necromancer (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  5 | 5+ | -- | 4+ |  1 | 11/13 | 110 |

* Special: Dark Surge (8), Heal (3), Individual, Zap! (3)

#### Necromancer options

* Mount on an undead horse, increasing Speed to 8 (+15 pts)

Twisted wizards who have dabbled uncautiously with the dark lore of
necromancy, these individuals have turned into unliving servants for
the lords of the undead.

### Liche King  (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  5 | 5+ | -- | 4+ |  1 | 14/16 | 180 |

* Special: Dark Surge (10), Heal (5), Individual, Zap! (5) 

#### Liche King options

* Mount on an undead horse, increasing Speed to 8 (+20 pts)

Ancient kings who prolonged their wicked lives using forbidden arts
until their bodies wasted away, the accursed Liche Kings are perhaps
the most skilled of undead sorcerers.

### Revenant King  (Hero)

| Unit Size                | Sp | Me | Ra | De | At | Nerve | Pts |
-------------------------- | :: | :: | :: | :: | :: | :---: | :-: |
1                          |  5 | 4+ | -- | 5+ |  5 | 14/16 | 120 |

* Special:  Crushing Strength (1), Dark Surge (6), Individual, Inspiring

#### Revenant King options

* Mount on an undead horse, increasing Speed to 8 (+15 pts)

Buried in their barrows together with their most precious weapons and
armour, the Revenant Kings make superb officers for the legions of
Skeletons and Revenants.

---

## Credits
   
* Written by: Alessio Cavatore.

* Artwork: Roberto Cirillo, Sascha Duis, Des Hanley, Phil Moss, Stef
Kopinski, Jonas Springborg, Matt Lee.

* Thanks to: Beasts of War, Richard Lambert, Chris Palmer, Tom Pugh
and the Chelmsford Bunker, Josh Roberts, Tabletop Insider... oh, and
Ronnie too.

* Liberated by: Matt Lee and Rob Myers, with eternal gratitude to
  Ronnie Renton.

* Modified by: (see below and add your name here)

* Scenery from owner's collection.

* Produced by: River Horse (Europe)
  Ltd. [http://www.riverhorse.eu](http://www.riverhorse.eu).

* Published by: Mantic Entertainment
  Ltd. [http://www.manticgames.com](http://www.manticgames.com).

* Mantic wants you to change this game! <http://warforge.org>.

* Copyright &copy; Mantic Entertainment 2011. ISBN: 978-0-9569945-0-9

* This work is licensed under the Creative Commons
Attribution-NonCommercial-ShareAlike 3.0 Unported License. To view a
copy of this license, visit
[http://creativecommons.org/licenses/by-nc-sa/3.0/](http://creativecommons.org/licenses/by-nc-sa/3.0/)
or send a letter to Creative Commons, 444 Castro Street, Suite 900,
Mountain View, California, 94041, USA.

* We are releasing the Kings of War rules in this manner to allow
players to better understand their relationship with us, and to be
able to apply their own ideas, modifications and designs to the game
outside of the scope of our game.

---

# Making your own game based on Kings of War

This is a short guide intended for people who want to alter and
release a new game based on the Kings of War ruleset.

Firstly, thank you for considering Kings of War as the basis of your
rules. We have tried to make a game that is simple to pick up and
difficult to master, while keeping the rules short and fluid. We hope
that your game will continue this legacy.

## Under the license chosen by Mantic, you can:

* Alter and improve upon the rules of Kings of War, including adapting
  them into new games and altering them to make variants of Kings of
  War (historical, aquatic, modern, etc).

* Publish your new rules: forums, blogs, wikis, mailing lists, copies
  at your local club -- even printed copies can be made, and you can
  charge a nominal fee for the price of copying (a pound, a dollar, a
  euro, etc)

* Use [official graphics and photography from the Mantic Games website](http://www.manticgames.com/Trade/Images/Kings-of-War-Downloads.html)
  to illustrate your ruleset, providing credit is given.

## Under the conditions:

* Commercial usage forbidden: Expressly, Mantic means commercial usage
  in the sense of selling copies of the rules. A commercial website
  such as Beasts of War is free to make their own rules and publish
  them for their readers, but games based on this ruleset cannot be
  sold. There is some consideration being given to removing this
  restriction in future releases on the rules, and if you're
  interested in seeing this happen, please email info@manticgames.com
  and give your feedback. One nice side-effect of removing this
  restriction is that games will be compatible with the licensing of
  Wikipedia, allowing games to include graphics and other materials
  from Wikipedia, as well as being publishable ON Wikipedia.

* Share and share-alike: **Your rules must carry the same license as
  these rules, and anyone receiving a copy of the rules must have the
  same freedoms as you regards modifying the rules.** Mantic also asks
  that you keep the original 'Credits' page intact, and add your own
  name or names to it, but do not remove any names or companies from
  the page.

* Please see the [human-readable license
  summary](http://creativecommons.org/licenses/by-nc-sa/3.0/) and
  [full legal
  code](http://creativecommons.org/licenses/by-nc-sa/3.0/legalcode)
  for all the details.

## Get the files to make your own version of Kings of War:

The core rules of Kings of War can be found at
<http://warforge.org/kow/3.0/> -- here, you can download the rules and
army lists in a variety of formats suitable for modification.

1. OpenDocument Format -- this is ideal if you want to casually modify
the rules in a simple Word Processing document. LibreOffice is an
office suite that probably exists for your operating system -- head
over to <http://libreoffice.org> and download a copy of this free
software. Once installed, you can simply open and change the
"KingsOfWar.odt" file you downloaded from Warforge.

2. HTML -- this format is more suitable to online publishing, but can
be easily edited in any text editor, or visual HTML editor. The HTML
version is supplied as a ZIP archive, containing all the images and
CSS files used for the online version of Warforge.

3. Markdown -- this is the ultimate version for any would-be game
hacker. This is the complete plain-text source file for Kings of War
that is used to generate the other files and the Warforge website. If
you're unfamiliar with running programs from a Command Prompt or
Terminal, you may struggle to use the Markdown version of Kings of War
effectively.

With any version of the game, the most important thing is to have fun
and make your own new game that you enjoy playing with friends! Do not
let a desire to produce something too fancy looking detract from
playing the game and rolling bucketloads of dice.

## Naming your new game:

* If your game is a variant of Kings of War, such as a Historical or
  Underwater version of the game, Mantic expressly permits you to
  include the name "Kings of War" in the title of your game, if you
  want to.

* For games unrelated to Kings of War, please *do not* use the Kings
  of War name in your game title.

## Credit where credit due!

* One more thing to do before you send out your great new game -- put
  YOUR name on it! Make sure that your version of Kings of War --
  whatever it may be called -- has the complete license and credit
  section left intact. You'll notice we left a space there for you! If
  you're modifying a version that someone else has already modified,
  simply add your name underneath theirs.

* Be sure to leave an email address or forum handle, and where
  possible use your real name, rather than just an alias. 

## Publishing your game:

* Once you're happy with your game rules, you might want to make it
look pretty. For this, we recommend [Scribus](http://scribus.org) --
Scribus is a piece of free software, available for Windows, Mac and
GNU/Linux, and will let you lay out text and graphics in an attractive
manner. It can make PDFs files suitable for littering the floor of
your local gaming store, and at a variety of sizes suitable for
obscuring the covers of even the heaviest of overpriced rulebooks. The
Markdown tool mentioned previously can also produce very stylish
output, but requires a little more technical knowledge.

## Your feedback

Please feel free to send me your feedback on the process of making
your own games based on Kings of War, either in the [Mantic
forum](http://forum.manticblog.com/), other forums (Username: mattl),
the Mantic subreddit --
[http://mantic.reddit.com](http://mantic.reddit.com) or in-person once
Kings of War rises up and becomes the most popular fantasy wargame
around. I hope you enjoy what represents eighteen months of emails,
phone calls, legal discussions, ramifications, 2am brainstorming,
private messages, pub chats, hangover chats, demo games and dice
rolling.

Matt Lee, Boston, MA -- June 2012.

PS. If you're reading past this point in the Markdown or HTML
versions, be prepared for a bunch of computer junk -- this actually
marks the document with the rights set out by Mantic. It's pretty
important!

<!-- // leave this here, please
<?xpacket begin='' id=''?>
<x:xmpmeta xmlns:x='adobe:ns:meta/'>
  <rdf:RDF xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'>
    <rdf:Description rdf:about=''
		     xmlns:xapRights='http://ns.adobe.com/xap/1.0/rights/'>
      <xapRights:Marked>True</xapRights:Marked>
    </rdf:Description>
    <rdf:Description rdf:about=''
		     xmlns:xapRights='http://ns.adobe.com/xap/1.0/rights/'
		     >
      <xapRights:UsageTerms>
	<rdf:Alt>

	  <rdf:li xml:lang='x-default' >This work is licensed under a &lt;a rel=&#34;license&#34; href=&#34;http://creativecommons.org/licenses/by-nc-sa/3.0/&#34;&gt;Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License&lt;/a&gt;.</rdf:li>
	  <rdf:li xml:lang='en_US' >This work is licensed under a &lt;a rel=&#34;license&#34; href=&#34;http://creativecommons.org/licenses/by-nc-sa/3.0/&#34;&gt;Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License&lt;/a&gt;.</rdf:li>

	  <rdf:li xml:lang='en' >This work is licensed under a &lt;a rel=&#34;license&#34; href=&#34;http://creativecommons.org/licenses/by-nc-sa/3.0/&#34;&gt;Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License&lt;/a&gt;.</rdf:li>
	  </rdf:Alt>
      </xapRights:UsageTerms>
    </rdf:Description>

    <rdf:Description rdf:about=''
      xmlns:cc='http://creativecommons.org/ns#'>
      <cc:license rdf:resource='http://creativecommons.org/licenses/by-nc-sa/3.0/'/>
    </rdf:Description>
    
  </rdf:RDF>
</x:xmpmeta>
<?xpacket end='r'?> // end of Kings of War rules, Happy Hacking -->
